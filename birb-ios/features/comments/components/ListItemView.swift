//
//  ListItemView.swift
//  birb-ios
//
//  Created by Michelle Broens on 17/12/2021.
//

import SwiftUI

struct ListItemView: View {
    var comment: Comment
    
    var body: some View {
        
        ZStack {
            Color.white
                .cornerRadius(15)
            commentInfoView
        }
        .fixedSize(horizontal: false, vertical: true)
        .shadow(color: Color.black.opacity(0.2), radius: 5, x: 0, y: 2)
    }
    
    var commentInfoView: some View {
        VStack(alignment: .leading, spacing: 4) {
            Text(comment.displayName)
                .font(.system(.body).bold())
            Text(comment.value)
                .font(.caption)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
        .padding(15)
    }
}

struct ListItemView_Previews: PreviewProvider {
    static var previews: some View {
        ListItemView(comment: Comment(displayName: "Bowie", value: "The animation Rio is a banger!")).previewLayout(.sizeThatFits)
    }
}
