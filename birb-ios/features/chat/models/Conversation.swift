//
//  Conversation.swift
//  birb-ios
//
//  Created by Michelle Broens on 12/01/2022.
//

import Foundation

struct Conversation: Hashable {
    let name: String
    let image: String
//    let lastMessage: String
}
