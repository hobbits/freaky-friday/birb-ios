//
//  AppStateModel.swift
//  birb-ios
//
//  Created by Michelle Broens on 05/01/2022.
//

import Foundation
import SwiftUI

import FirebaseAuth
import FirebaseFirestore

class AppStateModel: ObservableObject {
    @AppStorage("currentUsername") var currentUsername: String = ""
    @AppStorage("currentEmail") var currentEmail: String = ""
    
    @ObservedObject var friendStore = FriendStore()
    
    @Published var showSignIn: Bool = true
    @Published var conversations: [String] = []
    @Published var conversationsTest: [Conversation] = []
    @Published var messages: [Message] = []
    
    let database = Firestore.firestore()
    let auth = Auth.auth()
    
    var currentOtherUser = ""
    var conversationListener: ListenerRegistration?
    var chatListener: ListenerRegistration?
    
    init() {
        self.showSignIn = Auth.auth().currentUser == nil
    }
    
}

// search
extension AppStateModel {
    func searchUsers(queryText: String, completion: @escaping([User]) -> Void) {
        database.collection("users").getDocuments { snapshot, error in
            guard let usernames = snapshot?.documents.compactMap({ $0.documentID }),
                  error == nil else {
                      completion([])
                      return
                  }
            
            var users: [User] = []
            let filtered = usernames.filter({
                $0.lowercased().hasPrefix(queryText.lowercased())
            })
            
            for username in filtered {
                let image = self.getFriendImage(name: username)
                
                users.append(User(name: username, image: image))
            }

            completion(users)
        }
    }
}

// conversations
extension AppStateModel {
    func getConversations() {
        conversationListener = database
            .collection("users")
            .document(currentUsername)
            .collection("chats")
            .addSnapshotListener { snapshot, error in
                guard let usernames = snapshot?.documents.compactMap({ $0.documentID }),
                      error == nil else {
                        return
                }
                
                DispatchQueue.main.async {
                    self.conversations = usernames
                }
            }
    }
    
    func getConversationsTest() {
        conversationListener = database
            .collection("users")
            .document(currentUsername)
            .collection("chats")
            .addSnapshotListener { snapshot, error in
                guard let objects = snapshot?.documents.compactMap({ $0.data() }),
                      error == nil else {
                        return
                }
                
                let conversations: [Conversation] = objects.compactMap({

                    return Conversation(
                        name: $0["otherUsername"] as? String ?? "",
                        image: $0["image"] as? String ?? ""
                    )
                })
                DispatchQueue.main.async {
                    self.conversationsTest = conversations
                }
                
                print(self.conversationsTest)
            }
    }
}


// get chat / send messages
extension AppStateModel {
    func observeChat() {
        createConversation()
        chatListener = database
            .collection("users")
            .document(currentUsername)
            .collection("chats")
            .document(currentOtherUser)
            .collection("messages")
            .addSnapshotListener { snapshot, error in
                guard let objects = snapshot?.documents.compactMap({ $0.data() }),
                      error == nil else {
                        return
                }
                
                let messages: [Message] = objects.compactMap({
                    guard let date = ISO8601DateFormatter().date(from: $0["created"] as? String ?? "") else {
                        return nil
                    }
                    return Message(
                        text: $0["text"] as? String ?? "",
                        type: $0["sender"] as? String == self.currentUsername ? .sent : .received,
                        created: date)
                }).sorted(by: { first, second in
                    return first.created < second.created
                })
                
                DispatchQueue.main.async {
                    self.messages = messages
                }
            }
    }
    
    func sendMessage(text: String) {
        let newMessageId = UUID().uuidString
        let dateString = ISO8601DateFormatter().string(from: Date())
        
        let data = [
            "text": text,
            "sender": currentUsername,
            "created": dateString
        ]
        
        database.collection("users")
            .document(currentUsername)
            .collection("chats")
            .document(currentOtherUser)
            .collection("messages")
            .document(newMessageId)
            .setData(data)
        
        database.collection("users")
            .document(currentOtherUser)
            .collection("chats")
            .document(currentUsername)
            .collection("messages")
            .document(newMessageId)
            .setData(data)
        
    }
    
    func createConversation() {
        database.collection("users")
            .document(currentUsername)
            .collection("chats")
            .document(currentOtherUser)
            .setData(["otherUsername": currentOtherUser, "image": getFriendImage(name: currentOtherUser), "created": "true"])
        
        database.collection("users")
            .document(currentOtherUser)
            .collection("chats")
            .document(currentUsername)
            .setData(["otherUsername": currentUsername, "image": getFriendImage(name: currentUsername), "created": "true"])
    }
    
    func getFriendImage(name: String) -> String {
        self.friendStore.fetchFriends()
        var image = "https://cdn.shopify.com/s/files/1/2233/5237/products/QRKR-YLWa_1024x1024@2x.jpg?v=1504229356"
        
        for friend in friendStore.friends {
            if(friend.displayName == name) {
                image = friend.image
            }
        }
        
        return image
    }
}

// sign in & sign up users
extension AppStateModel {
    func signIn(username: String, password: String) {
        // try to sign in
        database.collection("users").document(username).getDocument { snapshot, error in
            guard let email = snapshot?.data()?["email"] as? String, error == nil else {
                return
            }
            
            self.auth.signIn(withEmail: email, password: password, completion: { result, error in
                guard result != nil, error == nil else {
                    return
            }
                
            DispatchQueue.main.async {
                self.currentUsername = username
                self.currentEmail = email
                self.showSignIn = false
            }
        })
    }
    }
    
    func signUp(email: String, username: String, password: String) {
        // try to sign up
        auth.createUser(withEmail: email, password: password) { result, error in
            guard result != nil, error == nil else {
                return
            }
        }
        
        // insert username into database
        let data = [
            "email": email,
            "username": username
        ]
        
        self.database
            .collection("users")
            .document(username)
            .setData(data) { error in
                guard error == nil else {
                    return
                }
            }
        
        DispatchQueue.main.async {
            self.currentUsername = username
            self.currentEmail = email
            self.showSignIn = false
        }
    }
    
    func signOut() {
        do {
            try auth.signOut()
            self.showSignIn = true
            self.currentUsername = ""
            self.currentEmail = ""
        }
        catch {
            print(error)
        }
    }
}
