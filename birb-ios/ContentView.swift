//
//  ContentView.swift
//  birb-ios
//
//  Created by Bebecca Rroens on 10/12/2021.
//

import SwiftUI
import TabBar

struct ContentView : View {
    @EnvironmentObject var model: AppStateModel
    @State private var selection: Item = .second
    
    var body: some View {
        NavigationView {
            TabBar(selection: $selection) {
                ConversationListView()
                    .tabItem(for: Item.first)

                BirbARView()
                    .tabItem(for: Item.second)
                
                FriendListView()
                    .tabItem(for: Item.third)
                }
            .tabBar(style: CustomTabBarStyle())
            .tabItem(style: CustomTabItemStyle())
        .fullScreenCover(isPresented: $model.showSignIn, content: {
            NavigationView {
                SignInView()
            }
    })
        }
    }
}

enum Item: Int, Tabbable {
    case first = 0
    case second = 1
    case third = 2
    
    var icon: String  {
        switch self {
            case .first:
            return "text.bubble.fill"
            case .second:
            return "house.fill"
            case .third:
            return "person.fill"
        }
    }
    
    var title: String  {
        switch self {
            case .first:
            return "Chat"
            case .second:
            return "Home"
            case .third:
            return "Profile"
        }
    }
}


#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
